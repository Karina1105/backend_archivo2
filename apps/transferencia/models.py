from django.db import models

from apps.fichaje.models import T_Especialidad
from apps.historia.models import Consulta
# Create your models here.
class OrdenTransferencia (models.Model):
    nro_orden = models.AutoField(primary_key = True)
    fecha = models.DateField
    servicio = models.CharField(max_length = 10)
    solicitud = models.CharField(max_length = 10)

class realiza_orden(models.Model):
    nro_orden = models.ForeignKey(OrdenTransferencia, on_delete= models.CASCADE)
    id_esp = models.ForeignKey(T_Especialidad, on_delete= models.CASCADE)
    id_consulta = models.ForeignKey(Consulta, on_delete = models.CASCADE)
