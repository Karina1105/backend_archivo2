from django.contrib import admin
from apps.personal.models import Unidad, PersonalU, T_Especialidad

# Register your models here.
admin.site.register(Unidad)
admin.site.register(PersonalU)
admin.site.register(T_Especialidad)