from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()


class Unidad(models.Model):
    nombre_u = models.CharField(max_length = 30)

    def __str__(self):
        return self.nombre_u
    class Meta:
        verbose_name_plural = ("Unidades")


class PersonalU(models.Model):
    Estado_CHOISES = (('Activo', 'Activo'),('Inactivo', 'Inactivo'))
    Sexo= (('F','Femenino'), ('M','Masculino'))
    carnet_i = models.CharField(max_length = 20, primary_key = True)
    p_apellido = models.CharField(max_length = 30)
    s_apellido = models.CharField(max_length = 30)
    nombres = models.CharField(max_length = 30)
    responsable = models.BooleanField(default = False)
    sexo = models.CharField(max_length = 1, choices = Sexo)
    estado = models.CharField(max_length = 10, choices = Estado_CHOISES, default = '', blank = True)
    unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    perfil = models.ImageField(upload_to = 'personal', blank = True, null = True)

    def __str__(self):
        return '%s %s' % (self.nombres, self.p_apellido)
    class Meta:
        verbose_name_plural = ("Personal")  
    
class T_Especialidad(models.Model):
    especialidad = models.CharField(max_length = 30)
    Atiende = (('PROMES','PROMES'),('SSU','SSU'))
    se_atiende = models.CharField(max_length =6, choices = Atiende)

    def __str__(self):
        return self.especialidad
    class Meta:
        verbose_name_plural = ("T_especialidades_medicas")

# class Personal(models.Model):
#     Estado_CHOISES = (('Activo', 'Activo'),('Inactivo', 'Inactivo'))
#     Genero_CHOISES= (('F','Femenino'), ('M','Masculino'))
#     ci = models.CharField(max_length = 20, primary_key = True)
#     primer_apellido = models.CharField(max_length = 30, blank = True)
#     segundo_apellido = models.CharField(max_length = 30, blank = True)
#     nombres = models.CharField(max_length = 30)
#     responsable = models.BooleanField(default = False)
#     genero = models.CharField(max_length = 1, choices = Genero_CHOISES)
#     estado = models.CharField(max_length = 10, choices = Estado_CHOISES, default = 'Inactivo', blank = True)
#     sector = models.CharField(max_length = 25)
#     item = models.CharField(max_length = 20, blank = True, null=True)
#     carga_horaria = models.CharField(max_length = 20)
#     cargo = models.CharField(max_length = 15)
#     especialidad = models.ForeignKey(T_Especialidad, on_delete = models.CASCADE,blank = True, null = True)
#     unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE)
#     usuario = models.ForeignKey(User, on_delete=models.CASCADE)
#     perfil = models.ImageField(upload_to = 'perfiles', blank = True, null = True)

#     def __str__(self):
#         return '%s %s' % (self.nombres, self.primer_apellido)
#     class Meta:
#         verbose_name_plural = ("Personal")  


class Personal(models.Model):
    Genero_CHOICES= ( ('Femenino', 'Femenino'), ('Masculino', 'Masculino') )
    Estado_CHOICES= (('Activo', 'Activo'),('Inactivo', 'Inactivo'))
    ci = models.CharField(primary_key= True, max_length = 15)
    nombres = models.CharField(max_length = 30)
    primer_apellido = models.CharField(max_length = 30,blank=True)
    segundo_apellido = models.CharField(max_length = 30,blank=True)
    genero= models.CharField(max_length=20, choices=Genero_CHOICES, default='',blank=True)
    item=models.CharField(max_length=20,blank=True)
    sector = models.CharField(max_length=50)
    responsable=models.BooleanField(default=False)
    cargo = models.CharField(max_length=50)
    carga_horaria = models.CharField(max_length=20)
    estado= models.CharField(max_length=15, choices=Estado_CHOICES, default='Inactivo',blank=True)
    foto_perfil=models.ImageField(upload_to='perfiles', blank=True, null=True)
    unidad=models.ForeignKey(Unidad,on_delete=models.CASCADE)
    usuario=models.OneToOneField(User,on_delete=models.CASCADE) # si se elimina el usuario se eliminara todo lo que se guardo con el
    especialidad = models.ForeignKey(T_Especialidad,  on_delete = models.CASCADE , blank = True, null = True)
    def _str_(self):
        return '%s %s' % (self.nombres, self.primer_apellido)
    class Meta:
        verbose_name_plural = ("Personal")