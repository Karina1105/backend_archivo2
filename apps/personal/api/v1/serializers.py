from rest_framework import serializers
from ....Usuarios_Login.serializers import UserSerializer
from ....personal.models import Unidad, PersonalU


class UnidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unidad
        fields = '__all__'

class PersonalUSerializer(serializers.ModelSerializer):
    usuario = UserSerializer(read_only=True)
    unidad = UnidadSerializer(read_only = True)
    class Meta:
        model = PersonalU
        fields = '__all__'