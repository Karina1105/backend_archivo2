from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404, render
from rest_framework import status
from django.db.models import Q
from ....personal.models import Unidad, PersonalU
from .serializers import UnidadSerializer, PersonalUSerializer
#,FichaAfiliadoSerializer,FichaAfiliadoDserializer
class ListaPersonal(APIView):
    def get(self, request, format = None):
        snippets = PersonalU.objects.all()
        serializer = PersonalUSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = PersonalUSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PersonalX(APIView):
    def get_object(self, pk):
        try:
            return PersonalU.objects.get(usuario__username = pk)
        except Personal.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PersonalUSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PersonalUSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
