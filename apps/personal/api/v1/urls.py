from django.urls import path
from .views import (
    ListaPersonal, PersonalX
)

app_name = 'personal'
urlpatterns = [
    path('LisPersonal/', ListaPersonal.as_view(), name = 'Lista del Personal'),
    path('PersonalX/<str:pk>/', PersonalX.as_view(), name = 'personal X'),
]