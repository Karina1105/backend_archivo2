from django.contrib import admin
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'personal'
urlpatterns = [
    path('v1/', include("apps.personal.api.v1.urls", namespace="v1")),
]
