from django.utils import timezone
from django.db import models
from ..afiliacion.models import Afiliado
from ..afiliacion.models import T_Tipo_Sangre
from ..afiliacion.models import T_Alergia_Medicamento
from ..fichaje.models import Medico
from ..fichaje.models import T_Especialidad

class Hist_Clinica(models.Model):
    codAfiliado = models.ForeignKey(Afiliado, max_length= 12, on_delete= models.CASCADE, primary_key = True)
    #alergiaMedicamentos = models.ManyToManyField(T_Alergia_Medicamento)

    def __str__(self):
        return self.codAfiliado

class T_Diagnostico(models.Model):
    id_diagnostico = models.CharField(primary_key = True, max_length = 10)
    diagnostico = models.CharField(max_length = 40)


class Consulta(models.Model):
    id_consulta = models.AutoField(primary_key = True)
    afiliado = models.ForeignKey(Afiliado, on_delete= models.CASCADE)
    observacion = models.CharField(max_length = 100)
    medico = models.ForeignKey(Medico, on_delete = models.CASCADE)
    fecha = models.DateField(default = timezone.now)

class ConsultaDiagnostico(models.Model):
    consulta = models.ForeignKey(Consulta, on_delete = models.CASCADE)
    diagnostico = models.ForeignKey(T_Diagnostico, on_delete = models.CASCADE)

class OrdenServicio(models.Model):
    id_ordens= models.AutoField(primary_key = True)
    consulta = models.ForeignKey(Consulta, on_delete = models.CASCADE)
    especialidad = models.ForeignKey(T_Especialidad, on_delete=models.CASCADE)

    def __str__(self):
        return self.especialidad

class OrdenTransferencia(models.Model):
    ESTADO = (('Espera','Espera'),('Enviado','Enviado'),('Regresado','Regresado'))
    id_ordent = models.AutoField(primary_key = True)
    consulta = models.ForeignKey(Consulta, on_delete = models.CASCADE)
    especialidad = models.ForeignKey(T_Especialidad, on_delete = models.CASCADE)
    estado = models.CharField(max_length = 10, choices = ESTADO, default = 'Espera')
    fechaida = models.DateField(blank = True, null = True)
    fechavuelta = models.DateField(blank = True, null = True)

    



# class T_Habito(models.Model):
#     id_habito = models.AutoField(primary_key= True)
#     habito = models.CharField(max_length = 20)

# class T_AntecedentePatologico(models.Model):
#     id_antecedente = models.AutoField(primary_key = True)
#     antecedenteP = models.CharField(max_length = 20)

# class T_AntecedenteGineco(models.Model):
#     id_antecedente = models.AutoField(primary_key = True)
#     antecedenteG = models.CharField(max_length = 20)

# class T_MetodoPlanificacion(models.Model):
#     id_metodo = models.AutoField(primary_key = True)
#     metodo = models.CharField(max_length = 20)

# class TieneAP(models.Model):
#     id_antecedente = models.ForeignKey(T_AntecedentePatologico, on_delete =models.CASCADE)
#     codAfiliado = models.ForeignKey(Hist_Clinica, on_delete= models.CASCADE)
#     tipo= models.CharField(max_length = 10)
#     obs= models.CharField(max_length = 30)

# class TieneAG(models.Model):
#     id_antecedente = models.ForeignKey(T_AntecedenteGineco, on_delete =models.CASCADE)
#     codAfiliado = models.ForeignKey(Hist_Clinica, on_delete= models.CASCADE)

# class UsaMPF(models.Model):
#     id_antecedente = models.ForeignKey(T_AntecedenteGineco, on_delete =models.CASCADE)
#     codAfiliado = models.ForeignKey(Hist_Clinica, on_delete= models.CASCADE)
#     obs= models.CharField(max_length = 30)

# # Create your models here.
