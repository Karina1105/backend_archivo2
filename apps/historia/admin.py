from django.contrib import admin
from apps.historia.models import Hist_Clinica, T_Diagnostico, Consulta, OrdenServicio, OrdenTransferencia
#, T_AntecedentePatologico,T_AntecedenteGineco,T_MetodoPlanificacion,TieneAG,TieneAP, UsaMPF, T_Habito

admin.site.register(Hist_Clinica)
admin.site.register(T_Diagnostico)
admin.site.register(Consulta)
admin.site.register(OrdenServicio)
admin.site.register(OrdenTransferencia)
# admin.site.register(T_AntecedentePatologico)
# admin.site.register(T_AntecedenteGineco)
# admin.site.register(T_MetodoPlanificacion)
# admin.site.register(TieneAG)
# admin.site.register(TieneAP)
# admin.site.register(UsaMPF)
# admin.site.register(T_Habito)

# Register your models here.
