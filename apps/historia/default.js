import React, { Component } from 'react';
import {Row, Card, ListGroupItem, CardHeader, Table,CardFooter, Button, CardBody, FormGroup, Label, Col, Input, } from 'reactstrap'
import Search from 'react-search-box';
import Lista from './Lista';
//import Historial from './Historial';

class Ordenest extends Component{
    constructor(props){
        super(props)
        this.state = {
            fecha:'',
            sw:false,

        }


        this.listar = this.listar.bind(this)
    }

    async componentDidMount(){
        try{
          var f=new Date()
          var d = f.getMonth()+1
            const a = await fetch(process.env.REACT_APP_API_URL+'afiliacion/v1/ListaAfiliados/',{
                headers: {
                  Authorization: `JWT ${localStorage.getItem('token')}`
                }
              })
            const afiliados = await a.json()
            const respuesta = await fetch(process.env.REACT_APP_API_URL+'fichaje/v1/Medicos/',{
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              }
            });
            const medicos = await respuesta.json();
              console.log(medicos)
            this.setState({
                afiliados,
                medicos,
                loading: false,
                mes:d,
                
              });
        }
        catch(e){
            console.log(e)
        }
    }


    listar(event){
      this.setState({
        sw:false,

      })
      if(this.state.ci !="")
        this.setState({
          
          sw:true,
          

        })
    }
    listarno(event){
      this.setState({
        swt:false,

      })
      if(this.state.ci !="")
        this.setState({
          
          sw:true,

        })
    }

    render(){
        if (this.state.loading) {
            return (
              <div className="">Esperando conexion...</div>
            );
          }
          
        return(
            <div>
                <Card style={{backgroundColor: '#e4e7ea'}} >
                    <CardHeader>
                        <h3><strong>Ingrese Fecha</strong></h3>
                    </CardHeader>
                    <br></br>
                    <Col md = "4">
                    </Col>
            
                <Col xs="12" md="12">
                <Label><strong>Ingrese:</strong> </Label>
                <br/>

                <Col md="4">
                  <Input 
                    type="date" name="mes" id="select" required  value={this.state.fecha} onChange={e => this.setState({ fecha: e.target.value })} />
                </Col>
                <Col md="3">
                
                <Button className="btn-spotify btn-brand icon mr-1 mb-1" onClick={this.listar.bind(this)}><strong>Mostrar</strong></Button>
                <Button className="btn-spotify btn-brand icon mr-1 mb-1" onClick={this.listarno.bind(this)}><strong>Mostrar</strong></Button>
                
                  
                </Col>

            </Col>
            </Card>
            { this.state.sw && <Lista f = {this.state.fecha}/> }
            { this.setState.swt && 
                      <Row>
                      <Card color="secondary" >
                          <CardHeader>
                              <h4><strong>Listado de Ordenes de Envio al SSU pendientes</strong></h4>
                          </CardHeader>
                          <CardBody>
                                            
                          <Table hover bordered striped responsive size="sm" >
                              <thead>
                                  <tr key={"1"}>
                                      <th style={{textAlign: 'center'}} ><strong>No</strong> </th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >Fecha</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >CI</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >Nombre y apellidos</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >Médico</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >Especialidad Destino</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >Acción</th>
                                  </tr>
                              </thead>
                              
                              <tbody>
                                  {
                                      this.state.listat.map((item,i) =>(
                                          <tr key={item.dia} >
                                          <th style={{textAlign: 'center'}} ><strong>{i}</strong> </th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >{item.consulta.fecha}</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >{item.consulta.afiliado.ci.ci}</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >{item.consulta.afiliado.ci.primer_apellido} {item.consulta.afiliado.ci.segundo_apellido} {item.consulta.afiliado.ci.nombres}</th>
                                      
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >{item.consulta.medico.apellido_p} {item.consulta.medico.nombre}</th>
                                      <th style={{backgroundColor: '#ffffff', textAlign: 'center'}} >{item.especialidad.especialidad}</th>
                                      <th> <Button  onClick={(e) => this.enviar(item.id_ordent, item.consulta.id_consulta, item.especialidad.id_especialidad)} color="primary" size="md" block>Enviar</Button> </th>
                                          </tr>
          
          
                                      ))
          
                                  }
                              </tbody>
                              </Table>
          
                      
                      </CardBody>
                      </Card>
          
                      <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                                  className={'modal-primary ' + this.props.className}>
                            <ModalHeader toggle={this.togglePrimary}>Mensaje</ModalHeader>
                            <ModalBody>
                                Historial Enviado!!!
                            </ModalBody>
                            <ModalFooter>
                              <Button color="primary" onClick={this.togglePrimary}>Aceptar</Button>{' '}
                            </ModalFooter>
                          </Modal>
                      </Row>
          }
            </div>
        )
    }


} 
export default Ordenest;