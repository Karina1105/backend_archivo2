from django.contrib import admin
from django.urls import path, include

app_name = 'historia'
urlpatterns = [
    path('v1/', include("apps.historia.api.v1.urls", namespace="v1")),
]
