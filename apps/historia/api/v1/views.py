# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
# Create your views here.
from rest_framework.views import APIView

from ...models import (
    Hist_Clinica, T_Diagnostico, Consulta, OrdenServicio, OrdenTransferencia, ConsultaDiagnostico
)
from .serializers import (
    Hist_ClinicaSerializer,
    T_DiagnosticoSerializer,
    ConsultaSerializer, OrdenServicioSerializer, OrdenTransferenciaSerializer, ConsultaDiagnosticoSerializer, ConsultaJSerializer, OrdenTransferenciaJSerializer,
)

class Historia(APIView):
    def get(self, request, format = None):
        snippets = Hist_Clinica.objects.all()
        serializer = Hist_ClinicaSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = Hist_ClinicaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Diagnostico(APIView):
    def get(sef, request, format = None):
        snippets = T_Diagnostico.objects.all()
        serializer = T_DiagnosticoSerializer (snippets, many = True)
        return Response(serializer.data)

class DiagnosticoXC(APIView):
    def get(sef, request, cod, format = None):
        snippets = T_Diagnostico.objects.filter(id_diagnostico = cod)
        serializer = T_DiagnosticoSerializer (snippets, many = True)
        return Response(serializer.data)

class DiagnosticoXD(APIView):
    def get(sef, request, des, format = None):
        snippets = T_Diagnostico.objects.filter(diagnostico = des)
        serializer = T_DiagnosticoSerializer (snippets, many = True)
        return Response(serializer.data)


class Consultas(APIView):
    def get(self, request, format = None):
        snippets = Consulta.objects.all()
        serializer = ConsultaSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = ConsultaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EstudianteConsulta(APIView):
    def get(self, request, pk, format=None):
        snippets = Consulta.objects.filter(afiliado = pk)
        serializer = ConsultaSerializer(snippets, many = True)
        return Response(serializer.data)

class ConsultaDiag(APIView):
    def get(self, request, format = None):
        snippets = ConsultaDiagnostico.objects.all()
        serializer = ConsultaDiagnosticoSerializer(snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = ConsultaDiagnosticoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrdenServ(APIView):
    def get(self, request, format = None):
        snippets = OrdenServicio.objects.all()
        serializer = OrdenServicioSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = OrdenServicioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OrdenTransf(APIView):
    def get(self, request, format = None):
        snippets = OrdenTransferencia.objects.all()
        serializer = OrdenTransferenciaSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = OrdenTransferenciaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrdenTransfX(APIView):
    def get(self, request, fecha, est, format = None):
        snippets = OrdenTransferencia.objects.filter(consulta__fecha = fecha, estado = est )
        serializer = OrdenTransferenciaJSerializer (snippets, many = True)
        return Response(serializer.data)

class OrdenTransT(APIView):
    def get(self, request, est, format = None):
        snippets = OrdenTransferencia.objects.filter(estado = est )
        serializer = OrdenTransferenciaJSerializer (snippets, many = True)
        return Response(serializer.data)

class OrdenTransPeriodo(APIView):
    def get(self, request, fecha1, fecha2, est, format = None):
        snippets = OrdenTransferencia.objects.filter(fechaida__range = [fecha1,fecha2], estado = est )
        serializer = OrdenTransferenciaJSerializer (snippets, many = True)
        return Response(serializer.data)


class OrdenTransfEdit(APIView):
    def get_object(self, pk):
        try:
            return OrdenTransferencia.objects.get(id_ordent=pk)
        except OrdenTransferencia.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenTransferenciaSerializer(snippet)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenTransferenciaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)