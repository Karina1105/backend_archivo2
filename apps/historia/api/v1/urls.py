from django.urls import path
from .views import (
    Historia, Diagnostico, Consultas, EstudianteConsulta, OrdenServ, OrdenTransf, ConsultaDiag, OrdenTransfX,OrdenTransfEdit, OrdenTransT, OrdenTransPeriodo,
)

app_name = 'historia'
urlpatterns = [

    path('HistoriaClinica/', Historia.as_view(), name = 'HisotiraPG'),
    path('TDianosticos/', Diagnostico.as_view(), name = 'Diagnostico'),
    path('Consulta/', Consultas.as_view(),  name = 'Consultas'),
    path('ConsultaDiagnosticos/', ConsultaDiag.as_view(), name = 'ConsultaDiagnostico'),
    path('ConsultaX/<str:pk>/', EstudianteConsulta.as_view(), name = 'ConsultaX'),
    path('OrdenServ/', OrdenServ.as_view(), name = 'Orden se servicio GP'),
    path('OrdenTransf/', OrdenTransf.as_view(), name = 'OrdendeTransferenciaGP'),
    path('OrdenTransT/<str:est>/', OrdenTransT.as_view(), name = 'OrdendeTransferenciaGP'),
    path('OrdenTransfX/<str:fecha>/<str:est>/', OrdenTransfX.as_view(), name = 'OrdendeTransferenciaGP'),
    path('OrdenTransPeriodo/<str:fecha1>/<str:fecha2>/<str:est>/', OrdenTransPeriodo.as_view(), name = 'OrdendeTransferenciapor periodo'),
    path('OrdenTransfE/<str:pk>/', OrdenTransfEdit.as_view(), name = 'OrdendeTransferencia editar'),
]