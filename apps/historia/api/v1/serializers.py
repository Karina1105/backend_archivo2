from rest_framework import serializers
from ....historia.models import  Hist_Clinica, Consulta, T_Diagnostico, OrdenServicio, OrdenTransferencia, ConsultaDiagnostico
from ....fichaje.models import Medico
from ....afiliacion.api.v1.serializers import AfiliadoJSerializer
from ....fichaje.api.v1.serializers import MedicoSerializer, EspecialidadSerializer

class Hist_ClinicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hist_Clinica
        fields = '__all__'

class T_DiagnosticoSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Diagnostico
        fields = '__all__'

class ConsultaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consulta
        fields = '__all__'


class ConsultaJSerializer(serializers.ModelSerializer):
    afiliado = AfiliadoJSerializer (read_only = True)
    medico = MedicoSerializer (read_only = True)
    class Meta:
        model = Consulta
        fields = '__all__'


class OrdenServicioSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdenServicio
        fields = '__all__'

class OrdenTransferenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdenTransferencia
        fields = '__all__'

class ConsultaDiagnosticoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConsultaDiagnostico
        fields = '__all__'

class OrdenTransferenciaJSerializer(serializers.ModelSerializer):
    consulta = ConsultaJSerializer (read_only = True)
    especialidad = EspecialidadSerializer(read_only = True)
    class Meta:
        model = OrdenTransferencia
        fields = '__all__'