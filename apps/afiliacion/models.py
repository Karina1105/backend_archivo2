from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.db import models

class Gestion(models.Model):
    id_g = models.AutoField(primary_key = True)
    gestion = models.CharField(max_length = 7)
    fecha = models.DateTimeField(default = timezone.now)

class Provincia(models.Model):
    id_prov= models.AutoField(primary_key = True)
    dprovincia = models.CharField(max_length = 30)

    def __str__(self):
        return self.dprovincia

    class Meta:
        verbose_name_plural = ("Provincia")

class Pais(models.Model):
    id_pais = models.AutoField(primary_key = True)
    pais = models.CharField(max_length = 15)

    def __str__(self):
        return self.pais

    class Meta:
        verbose_name_plural = ("Paises")

class Facultad(models.Model):
    id_fac = models.AutoField(primary_key = True)
    facultad = models.CharField(max_length = 40)

    def __str__(self):
        return self.facultad

    class Meta:
        verbose_name_plural = ("Facultades")

class Carrera(models.Model):
    id_carrera = models.AutoField(primary_key  = True)
    carrera  = models.CharField(max_length = 50)
    programa = models.CharField(max_length = 15)
    facultad = models.ForeignKey(Facultad, on_delete= models.CASCADE)


    def __str__(self):
        return self.carrera

    class Meta:
        verbose_name_plural = ("Carreras")

class T_Estado_Civil(models.Model):
    id_estado = models.AutoField(primary_key=True)
    estado = models.CharField(max_length = 15)

    def __str__(self):
        return self.estado

    class Meta:
        verbose_name_plural = ("T_Estado_Civil")

class T_Parentesco(models.Model):
    parentesco = models.CharField(max_length = 20)

    def __str__(self):
        return self.parentesco

class Estudiante (models.Model):
    Sexo= (('F','Femenino'), ('M','Masculino'))
    ci = models.CharField(primary_key= True, max_length = 15, )
    nombres = models.CharField(max_length = 30)
    primer_apellido = models.CharField(max_length = 30, blank=True, null = True)
    segundo_apellido = models.CharField(max_length = 30, blank=True, null = True)
    sexo = models.CharField(max_length = 1, choices = Sexo)
    fecha_nac = models.DateField()
    telefono = models.IntegerField(null = True,blank = True)
    celular = models.IntegerField()
    direccion = models.CharField(max_length = 50)
    email = models.CharField(max_length = 25, default = '', blank = True)
    est_civil = models.ForeignKey(T_Estado_Civil, on_delete = models.CASCADE)
    Carrera = models.ForeignKey(Carrera, on_delete= models.CASCADE )
    matricula = models.PositiveIntegerField()


    def __str__(self):
        return str(self.ci)

    def __unicode__(self):
        return self.ci

    class Meta:
        verbose_name_plural = ("Estudiantes")

class T_Ciudad(models.Model):
    id_ciudad= models.AutoField(primary_key = True)
    des_ciudad = models.CharField(max_length=30)

    def __str__(self):
        return self.des_ciudad

    class Meta:
        verbose_name_plural = ("T_Ciudad")

class T_Alergia_Medicamento(models.Model):
    id_alergia = models.AutoField(primary_key = True)
    alergia_med = models.CharField(max_length=30)

    def __str__(self):
        return self.alergia_med

    class Meta:
        verbose_name_plural = ("T_Alergia_Medicamento")

class T_Tipo_Sangre(models.Model):
    id_Sangre = models.AutoField(primary_key = True)
    sangre = models.CharField(max_length = 10)

    def __str__(self):
        return self.sangre

    class Meta:
        verbose_name_plural = ("T_Tipo_Sangre")

class T_Respaldo(models.Model):
    id_respaldo = models.AutoField(primary_key = True)
    respaldo = models.CharField(max_length = 15)

    def __str__(self):
        return self.respaldo

    class Meta:
        verbose_name_plural = ("tabla respaldo")

class Afiliado(models.Model):
    ESTADO = (
        ('Activo','Activo'),
        ('Inactivo','Inactivo'),
        ('Pendiente','Pendiente')
    )
    ci = models.ForeignKey(Estudiante,on_delete = models.CASCADE, unique = True)
    codAfiliado = models.CharField(max_length = 14 , primary_key = True)
    apellidoEsposo = models.CharField(max_length = 20, blank = True )
    nombre_Referencia = models.CharField(max_length = 50)
    telefono_Referencia = models.PositiveIntegerField()
    parentesco = models.ForeignKey(T_Parentesco, blank = True, on_delete = models.CASCADE, null = True)
    ciudad = models.ForeignKey(T_Ciudad, on_delete = True)
    estado = models.CharField(max_length = 10, choices = ESTADO, default = 'Inactivo')
    provnac= models.ForeignKey(Provincia, on_delete = models.CASCADE, blank = True)
    foto = models.ImageField(upload_to = 'afiliados', blank = True, null = True)
    tipo_sangre = models.ForeignKey(T_Tipo_Sangre, blank = True, null = True, on_delete = models.CASCADE, )
    respaldo = models.ForeignKey(T_Respaldo, blank = True, null = True, on_delete= models.CASCADE)
    fecha = models.DateField(default = timezone.now)
    gestion = models.IntegerField(default = 2019)

   
    def __str__(self):
        return str(self.ci)

    class Meta:
        verbose_name_plural = ("Afiliado")

class Registro_aler(models.Model):
    afiliado = models.ForeignKey(Afiliado, on_delete = models.CASCADE)
    alergia = models.ForeignKey(T_Alergia_Medicamento, on_delete = models.CASCADE)
    personal = models.IntegerField()

    class Meta:
        verbose_name_plural = ("Registro_alergias")

class T_Localidad(models.Model):
    id_localidad = models.AutoField(primary_key = True)
    des_localidad = models.CharField(max_length=30)

    def __int__(self):
        return self.id_localidad

    class Meta:
        verbose_name_plural = ("T_Localidad")

class FORM_DOC(models.Model):
    """Modelo para registrar_visita usuario"""
    cod_form = models.AutoField(primary_key = True)
    ci = models.ForeignKey(Estudiante, on_delete = models.CASCADE)
    afp_f = models.BooleanField(default = False, verbose_name=("AFP Futuro"))
    afp_p = models.BooleanField(default = False, verbose_name=("AFP Prevision"))
    for110 =  models.BooleanField(default = False, verbose_name=("Declaracion Jurada"))
    matriculaf = models.BooleanField(default= False, verbose_name=("Fot. matricula"))
    cif = models.BooleanField(default= False, verbose_name=("fot. carnet"))
    #certnac = models.BooleanField(default= False, verbose_name=("Fot. Certificado nac."))
    boletains = models.BooleanField(default= False, verbose_name=("fot. Boleta de Inscripcion"))
    #fotografia = models.BooleanField(default= False, verbose_name=("Fotografia"))
    carrera = models.ForeignKey(Carrera, on_delete = models.CASCADE)
    fecha = models.DateTimeField(default = timezone.now)
    gestion = models.IntegerField(default = 2017)

    def __int__(self):
        return self.cod_form

    def __unicode__(self):
        return self.cod_form

    class Meta:
        verbose_name_plural = ("Formulario Afiliacion")

class RFORM_DOC(models.Model):
    """Modelo para registrar reafiliaciones"""
    cod_forr = models.AutoField(primary_key = True)
    ci = models.ForeignKey(Estudiante, on_delete = models.CASCADE)
    afp_f = models.BooleanField(default = False, verbose_name=("AFP Futuro"))
    afp_p = models.BooleanField(default = False, verbose_name=("AFP Prevision"))
    for110 =  models.BooleanField(default = False, verbose_name=("Declaracion Jurada"))
    matriculaf = models.BooleanField(default= False, verbose_name=("Fot. matricula"))
    cif = models.BooleanField(default = False, verbose_name = ("Fot. de Carnet de identidad") )
    boletains = models.BooleanField(default= False, verbose_name=("Fot. Boleta de Inscripcion"))
    #fotografia = models.BooleanField(default= False, verbose_name=("Fotografia"))
    carrera = models.ForeignKey(Carrera, on_delete = models.CASCADE)
    fecha = models.DateTimeField(default = timezone.now)
    gestion = models.IntegerField(default = 2019)

    def __int__(self):
        return self.cod_forr

    def __unicode__(self):
        return self.cod_forr

    class Meta:
        verbose_name_plural = ("Formulario Reafiliacion")

class T_Baja(models.Model):
    id_baja = models.AutoField(primary_key = True)
    baja = models.CharField(max_length = 30)
    #solicita = models.ManyToManyField('Afiliado', through = 'SolicitaBaja')

    def __str__(self):
        return self.baja

    class Meta:
        verbose_name_plural = ("T_Baja")

class SolicitaBaja(models.Model):
    codAfiliado = models.ForeignKey(Afiliado, on_delete=models.CASCADE)
    id_baja = models.ForeignKey(T_Baja,on_delete=models.CASCADE)
    observacion = models.CharField(max_length = 30,blank = True )
    fecha = models.DateField()

    def __unicode__(self):
        return self.ci

    class Meta:
        verbose_name_plural = ("SolicitudBaja")


################################################

class Estudianteumsa(models.Model):
    dip = models.CharField(max_length = 15)
    id_programa = models.CharField(max_length = 7)
    id_estudiante = models.IntegerField(primary_key=True)
    primer_apellido	= models.CharField(max_length = 20, blank = True, null = True)
    segundo_apellido = models.CharField(max_length = 20, blank = True, null = True)
    nombres	=models.CharField(max_length = 30)
    telefono = ArrayField(models.IntegerField(), blank = True)
    correo	= models.CharField(max_length = 30, blank=True, null = True)
    direccion = models.CharField(max_length = 50, blank=True, null = True)
    fec_nacimiento = models.DateField()
    estado_estudiante = models.CharField(max_length = 40)
    sede = models.CharField(max_length = 20)
    id_sede	= models.IntegerField()
    matricula = models.IntegerField()
    id_sexo	= models.CharField(max_length = 1)
    estado_civil = models.CharField(max_length = 10)
    provincia = models.CharField(max_length = 30)
    tieneSeguroActual = models.BooleanField()
    gestionSeguro = models.IntegerField()
    tieneCertificadoNoDeudas = models.BooleanField()
    gestionCertificadoNoDeudas = models.IntegerField()

    def __str__(self):
        return self.dip

    class Meta:
        verbose_name_plural = ("EstUMSA")