from django.contrib.auth.models import User
from rest_framework import serializers

from ....afiliacion.models import (
    FORM_DOC,
    RFORM_DOC,
    Afiliado,
    Carrera,
    Facultad,
    Estudiante,
    Provincia,
    T_Alergia_Medicamento,
    T_Ciudad,
    T_Estado_Civil,
    T_Tipo_Sangre,
    Registro_aler,
    T_Baja,
    SolicitaBaja,
    T_Respaldo,
    Pais,
    Estudianteumsa,
    T_Parentesco,
)

class SangreSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Tipo_Sangre
        fields = '__all__'

class RespaldoSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Respaldo
        fields = '__all__'


class FacultadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facultad
        fields = '__all__'

class CarreraSerializer(serializers.ModelSerializer):
    facultad = FacultadSerializer(read_only = True)
    class Meta:
        model = Carrera
        fields = '__all__'

class CarreraJSerializer(serializers.ModelSerializer):
    facultad = FacultadSerializer(read_only = True)
    class Meta:
        model = Carrera
        fields = '__all__'

class PaisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pais
        fields = '__all__'

class ProvinciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provincia
        fields = '__all__'

class CiudadSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Ciudad
        fields = '__all__'

class ECivilSerializer(serializers.ModelSerializer):
	
    class Meta:
        model = T_Estado_Civil
        fields = '__all__'

class ParentescoSerializer(serializers.ModelSerializer):

    class Meta:
        model = T_Parentesco
        fields = '__all__'

class EstudiantesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estudiante
        fields = '__all__'

class EstudianteJSerializer(serializers.ModelSerializer):
    est_civil = ECivilSerializer(read_only = True)
    Carrera = CarreraJSerializer(read_only = True)
    class Meta:
        model = Estudiante
        fields = '__all__'
##########################################################
class umsaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estudianteumsa
        fields = '__all__'
##########################################################

class AfiliadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Afiliado
        fields = '__all__'


class EstudianteSexoSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField()
    class Meta:
        model = Estudiante
        fields = ('sexo','total')

class EstudianteTSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField()
    class Meta:
        model = Estudiante
        fields = ('Carrera_id','total')

class ETSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField()
    ci = EstudiantesSerializer(read_only = True)
    carrera = CarreraSerializer(read_only = True)
    class Meta:
        model = Afiliado
        fields = ('carrera','total')

class AfiliadoJSerializer(serializers.ModelSerializer):
    ci = EstudianteJSerializer(read_only = True)
    ciudad = CiudadSerializer(read_only = True)
    provnac = ProvinciaSerializer(read_only = True)
    pais = PaisSerializer(read_only = True)
    tipo_sangre = SangreSerializer(read_only = True)
    respaldo = RespaldoSerializer(read_only = True)
    parentesco = ParentescoSerializer(read_only = True)
    class Meta:
        model = Afiliado
        fields = '__all__'



class FORM_DOCSerializer(serializers.ModelSerializer):
    class Meta:
        model = FORM_DOC
        fields = '__all__'

class RFORM_DOCSerializer(serializers.ModelSerializer):
    class Meta:
        model = RFORM_DOC
        fields = '__all__'

class FORM_DOCDSerializer(serializers.ModelSerializer):
    carrera = CarreraJSerializer(read_only = True)
    class Meta:
        model = FORM_DOC
        fields = '__all__'

class RFORM_DOCDSerializer(serializers.ModelSerializer):
    carrera = CarreraJSerializer(read_only = True)
    class Meta:
        model = RFORM_DOC
        fields = '__all__'

# class FormaAfiSerializer(serializers.ModelSerializer):
#   ci = AfiliadoSerializer(read_only = True)
# 	class Meta:
# 		model =FORM_DOC
# 		fields = '__all__'
			
# ""
# class RegistroAlergiasSerializer(serializers.ModelSerializer):
#     #afiliado = AfiliadoSerializer(read_only = True)
#     #alergia = T_Alergia_Medicamento(read_only = True)

#     class Meta:
#         model = Registro_aler
#         fields = '__all__'

class TBajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Baja
        fields = '__all__'

class SolicitaBajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolicitaBaja
        fields = '__all__'

class VerSolicitaBajaSerializer(serializers.ModelSerializer):
    codAfiliado = AfiliadoJSerializer(read_only = True)
    id_baja = TBajaSerializer(read_only = True)
    class Meta:
        model = SolicitaBaja
        fields = '__all__'
