# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.parsers import  MultiPartParser, FormParser, FileUploadParser
from rest_framework.decorators import detail_route
from rest_framework.response import Response
# Create your views here.
from rest_framework.views import APIView
# from django.db.models.lookups import MonthTransform as Month, YearTransform as Year
from django.db.models import Avg, Count, Min, Sum

from ...models import (
    FORM_DOC,
    RFORM_DOC,
    Afiliado,
    Carrera,
    Facultad,
    Estudiante,
    Provincia,
    T_Ciudad,
    T_Tipo_Sangre,
    T_Alergia_Medicamento,
    Registro_aler,
    T_Estado_Civil,
    SolicitaBaja,
    T_Respaldo,
    Pais,
    T_Baja, 
    T_Parentesco,
    SolicitaBaja,
    Estudianteumsa
    )
from .serializers import (
    SangreSerializer,
    RespaldoSerializer,
    CarreraSerializer,
    FacultadSerializer,
    PaisSerializer,
    ProvinciaSerializer,
    CiudadSerializer,
    ECivilSerializer,
    EstudiantesSerializer, EstudianteJSerializer, EstudianteTSerializer, EstudianteSexoSerializer,
    AfiliadoSerializer, AfiliadoJSerializer, ParentescoSerializer,
    FORM_DOCSerializer,
    RFORM_DOCSerializer,
    umsaSerializer,
    TBajaSerializer, SolicitaBajaSerializer, VerSolicitaBajaSerializer,
    FORM_DOCDSerializer, RFORM_DOCDSerializer,

)
class ListaPais(APIView):
    def get(self, request, format = None):
        snippets = Pais.objects.all()
        serializer = PaisSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = PaisSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaFacultad(APIView):
    def get(self, request, format = None):
        snippets = Facultad.objects.all()
        serializer = FacultadSerializer(snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer =  FacultadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaCarrera(APIView):
    def get(self, request):
        carrera = Carrera.objects.all()
        data = CarreraSerializer(carrera, many=True).data
        return Response(data)

class ListaProvincia(APIView):
    def get(self, request):
        provincia = Provincia.objects.all()[:20]
        data = ProvinciaSerializer(provincia, many=True).data
        return Response(data)

class ListaSangre(APIView):
    def get(self, request, format = None):
        snippets = T_Tipo_Sangre .objects.all()
        serializer = SangreSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = SangreSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class LParentesco(APIView):
    def get(self, request, format = None):
        snippets = T_Parentesco.objects.all()
        serializer = ParentescoSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = ParentescoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaRespaldo(APIView):
    def get(self, request, format = None):
        snippets = T_Respaldo.objects.all()
        serializer = RespaldoSerializer(snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = RespaldoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class ListaCiudad(APIView):
    def get(self, request, format = None):
        snippets = T_Ciudad.objects.all()
        serializer = CiudadSerializer (snippets, many = True)
        return Response(serializer.data)


    def post(self, request, format=None):
        serializer = CiudadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"datos de Estado Civil"
class LEstadoCivil(APIView):

    def get(self, request, format=None):
        snippets = T_Estado_Civil.objects.all()
        serializer = ECivilSerializer(snippets, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        serializer = ECivilSerializer (data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Listaumsa(APIView):
    def get(self, request, format=None):
        snippets = Estudianteumsa.objects.all()
        serializer = umsaSerializer(snippets, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        serializer = umsaSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class umsaest(APIView):
    def get(self, request, pk, format=None):
        snippets = Estudianteumsa.objects.filter(dip = pk)
        serializer = umsaSerializer(snippets, many = True)
        return Response(serializer.data)

class umsaestpmn(APIView):
    def get(self, request, ap1, ap2, nom, format=None):
        snippets = Estudianteumsa.objects.filter(primer_apellido = ap1).filter(segundo_apellido = ap2).filter(nombres = nom)
        serializer = umsaSerializer(snippets, many = True)
        return Response(serializer.data)

class umsaestpn(APIView):
    def get(self, request, ap1, nom, format=None):
        snippets = Estudianteumsa.objects.filter(primer_apellido = ap1).filter(segundo_apellido = ap1).filter(nombres = nom)
        serializer = umsaSerializer(snippets, many = True)
        return Response(serializer.data)

class ListaEstudiantes(APIView):
    def get(self, request, format=None):
        snippets = Estudiante.objects.all()
        serializer = EstudiantesSerializer(snippets, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        serializer = EstudiantesSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status= status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EstudianteEditar(APIView):
    def get_object(self, pk):
        try:
            return Estudiante.objects.get(ci=pk)
        except Estudiante.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = EstudiantesSerializer(snippet)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = EstudiantesSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class EstudianteDetalle(APIView):

    def get(self, request, pk, format=None):
        snippets = Estudiante.objects.filter(ci = pk)
        serializer = EstudianteJSerializer(snippets, many = True)
        return Response(serializer.data)


class ListaAfiliado(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get(self, request, *args, **kwargs):
        snippets = Afiliado.objects.all().order_by('-fecha')
        serializer = AfiliadoSerializer (snippets, many = True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = AfiliadoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SumAfiliados(APIView):
    def get(self, request,g, format=None):
        snippets = Afiliado.objects.filter(gestion = g)
        serializer = AfiliadoSerializer(snippets, many = True)
        return Response(serializer.data)

class SumReAf(APIView):
    def get(self, request,g, format=None):
        snippets = RFORM_DOC.objects.filter(gestion = g)
        serializer = RFORM_DOCSerializer(snippets, many = True)
        return Response(serializer.data)

class SumAfNu(APIView):
    def get(self, request,g, format=None):
        snippets = FORM_DOC.objects.filter(gestion = g)
        serializer = FORM_DOCSerializer(snippets, many = True)
        return Response(serializer.data)


class DLAfiliado(APIView):
    def get(self, request, format=None):
        snippets = Afiliado.objects.all().order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)


class EditaAfiliado(APIView):
     
    def get_object(self, pk):
        try:
            return Afiliado.objects.get(codAfiliado=pk)
        except Afiliado.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AfiliadoSerializer(snippet)
        return Response(serializer.data)

    # parser_classes = (MultiPartParser,FormParser)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AfiliadoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AfiliadoX(APIView):
    def get(self, request, pk, format=None):
        snippets = Afiliado.objects.filter(ci = pk)
        serializer = AfiliadoSerializer(snippets, many = True)
        return Response(serializer.data)

class Afiliadocod(APIView):
    def get(self, request, pk, format=None):
        snippets = Afiliado.objects.filter(codAfiliado = pk)
        serializer = AfiliadoSerializer(snippets, many = True)
        return Response(serializer.data)


class DetalleAfiliado(APIView):
    def get(self, request, pk, format=None):
        snippets = Afiliado.objects.filter(ci = pk).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class DetalleAfiliadocod(APIView):
    def get(self, request, pk, format=None):
        snippets = Afiliado.objects.filter(codAfiliado = pk).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class FormAfiliacion(APIView):
    
    def get(self, request, format = None):
        snippets = FORM_DOC.objects.all().order_by('-fecha')
        serializer = FORM_DOCSerializer (snippets, many = True)
        return Response(serializer.data)
    def post(self, request, format=None):
        serializer = FORM_DOCSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FormReAfiliacion(APIView):

    def get(self, request, format = None):
        snippets = RFORM_DOC.objects.all().order_by('-fecha')
        serializer = RFORM_DOCSerializer(snippets, many = True)
        return Response(serializer.data)

    def post(self, request, format = None):
        serializer = RFORM_DOCSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class FormAfiliacionX(APIView):
    
    def get_object(self, pk):
        try:
            return FORM_DOC.objects.get(ci=pk)
        except Estudiante.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FORM_DOCDSerializer(snippet)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FORM_DOCSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class FormReAfiliacionX(APIView):
    def get(self, request, pk, format=None):
        snippets = RFORM_DOC.objects.filter(ci = pk).order_by('-fecha')
        serializer = RFORM_DOCDSerializer(snippets, many = True)
        return Response(serializer.data)
    # def get_object(self, pk):
    #     try:
    #         return RFORM_DOC.objects.get(ci=pk)
    #     except Estudiante.DoesNotExist:
    #         raise Http404


    # def get(self, request, pk, format=None):
    #     snippet = self.get_object(pk)
    #     serializer = RFORM_DOCSerializer(snippet)
    #     return Response(serializer.data)


    # def put(self, request, pk, format=None):
    #     snippet = self.get_object(pk)
    #     serializer = RFORM_DOCSerializer(snippet, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, pk, format=None):
    #     snippet = self.get_object(pk)
    #     snippet.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)

class AfiliadoDia(APIView):
    def get(self, request,fecha,format=None):
        snippets = Afiliado.objects.filter(fecha = fecha).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)


class AfiliadodelDia(APIView):
    def get(self, request,fecha, est,format=None):
        snippets = Afiliado.objects.filter(estado = est, fecha = fecha).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class AfiliacionPeriodoT(APIView):
    def get(self, request,fecha1,fecha2,format=None):
        snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2]).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class AfiliacionPeriodo(APIView):
    def get(self, request,fecha1,fecha2,est,format=None):
        snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2], estado= est).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class AfiliacionSexo(APIView):
    def get(self, request,fecha1,fecha2,format=None):
        res = Afiliado.objects.filter(fecha__range=[fecha1, fecha2])
        lista = res.values_list('ci', flat=True)
        snippets = Estudiante.objects.filter(ci__in=lista).values('sexo').annotate(total=Count('sexo')).order_by('total')
        serializer = EstudianteSexoSerializer(snippets, many = True)
        return Response(serializer.data)


"Carrera"
class AfiliacionTCarrera(APIView):
    def get(self, request,fecha1,fecha2,format=None):
        res = Afiliado.objects.filter(fecha__range=[fecha1, fecha2])
        lista = res.values_list('ci', flat=True)
        snippets = Estudiante.objects.filter(ci__in=lista).values('Carrera_id').annotate(total=Count('Carrera')).order_by('total')
        serializer = EstudianteTSerializer(snippets, many = True)
        return Response(serializer.data)

class AfiliacioncarreraT(APIView):
    def get(self, request,fecha1,fecha2,carrera,format=None):
        
        snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2], ci__Carrera__carrera= carrera).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class Afiliacioncarrera(APIView):
    def get(self, request,fecha1,fecha2,est, carrera,format=None):
        snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2], estado= est, ci__Carrera__carrera = carrera).order_by('-fecha')
        serializer = AfiliadoJSerializer(snippets, many = True)
        return Response(serializer.data)

class TBajas(APIView):
    def get(self, request,format=None):
        snippet = T_Baja.objects.all()
        serializador = TBajaSerializer(snippet, many=True)
        return Response(serializador.data)
 #Crea un nuevo registro
    def post(self, request, format=None):
        serializer = TBajaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    #Baja Detallada DE un Afiliado

class VerBajaAfiliado(APIView):
    def get(self, request, pk,format=None):
        snippet = SolicitaBaja.objects.filter(ci=pk)
        serializador = VerSolicitaBajaSerializer(snippet, many=True)
        return Response(serializador.data)
 ## obtener y crear un nuevo registro de Baja deafiliado
class SolicitarBaja(APIView):
    def get(self, request,format=None):
        snippet = SolicitaBaja.objects.all()
        serializador = SolicitaBajaSerializer(snippet, many=True)
        return Response(serializador.data)
 #Crea un nuevo registro
    def post(self, request, format=None):
        serializer = SolicitaBajaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class BajasdelDia(APIView):
    def get(self, request,fecha,format=None):
        snippets = SolicitaBaja.objects.filter( fecha = fecha)
        serializer = VerSolicitaBajaSerializer(snippets, many = True)
        return Response(serializer.data)

class BajasPeriodo(APIView):
    def get(self, request,fecha1,fecha2,format=None):
        snippets = SolicitaBaja.objects.filter(fecha__range=[fecha1, fecha2])
        serializer = VerSolicitaBajaSerializer(snippets, many = True)
        return Response(serializer.data)

## obtiene, edita y elimina un registro de baja de afiliado

class VerEditarBaja(APIView):
    def get_object(self, pk):
        try:
            return SolicitaBaja.objects.get(ci=pk)
        except Estudiante.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SolicitaBajaSerializer(snippet)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = SolicitaBajaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
