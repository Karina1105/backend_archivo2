from django.contrib import admin
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'afiliacion'
urlpatterns = [
    path('v1/', include("apps.afiliacion.api.v1.urls", namespace="v1")),
]
