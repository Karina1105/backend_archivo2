# # -*- coding: utf-8 -*-
# from __future__ import unicode_literals

# from django.shortcuts import get_object_or_404, render
# from rest_framework import status, viewsets
# from rest_framework.decorators import detail_route
# from rest_framework.response import Response
# # Create your views here.
# from rest_framework.views import APIView

# from .models import (
#     FORM_DOC,
#     Afiliado,
#     Carrera,
#     Estudiante,
#     Provincia,
#     T_Ciudad,
#     T_Tipo_Sangre,
#     T_Alergia_Medicamento,
#     Registro_aler,
#     T_Estado_Civil,
#     SolicitaBaja,
#     T_Baja,
#     )
# from .serializers import (
#     AfiliadoSerializer,
#     CarreraSerializer,
#     CiudadSerializer,
#     DAfiliadoSerializer,
#     EstudiantesSerializer,
#     EstudianteSerializer,
#     FORM_DOCSerializer,
#     ProvinciaSerializer,
#     SangreSerializer,
#     AlergiasSerializer,
#     RegistroAlergiasSerializer,
#     FormaAfiSerializer,
#     ECivilSerializer,
#     TBajaSerializer,
#     VerSolicitaBajaSerializer,
#     SolicitaBajaSerializer,
# )


# class ListaCarrera(APIView):
#     def get(self, request):
#         carrera = Carrera.objects.all()[:20]
#         data = CarreraSerializer(carrera, many=True).data
#         return Response(data)



# class ListaProvincia(APIView):
#     def get(self, request):
#         provincia = Provincia.objects.all()[:20]
#         data = ProvinciaSerializer(provincia, many=True).data
#         return Response(data)

# class ListaSangre(APIView):
#     def get(self, request, format = None):
#         snippets = T_Tipo_Sangre .objects.all()
#         serializer = SangreSerializer (snippets, many = True)
#         return Response(serializer.data)


#     def post(self, request, format=None):
#         serializer = SangreSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class ListaEstudiantes(APIView):
#     def get(self, request):
#         estudiante = Estudiante.objects.all()[:20]
#         data = EstudiantesSerializer(estudiante, many = True).data
#         return Response(data)


# class ListaCiudad(APIView):
#     def get(self, request, format = None):
#         snippets = T_Ciudad .objects.all()
#         serializer = CiudadSerializer (snippets, many = True)
#         return Response(serializer.data)


#     def post(self, request, format=None):
#         serializer = CiudadSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# "datos de Estado Civil"
# class EstCivil(APIView):
#     def get(self, request, format=None):
#         snippets = T_Estado_Civil.objects.all()
#         serializer = ECivilSerializer(snippets, many = True)
#         return Response(serializer.data)

#     def post(self, request, format = None):
#         serializer = ECivilSerializer (data = request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status= status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class ListaAlergia(APIView):
#     def get(self, request, format = None):
#         snippets = T_Alergia_Medicamento .objects.all()
#         serializer = AlergiasSerializer(snippets, many = True)
#         return Response(serializer.data)


#     def post(self, request, format=None):
#         serializer = AlergiasSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class EditaAfiliado(APIView):
#     def get_object(self, pk):
#         try:
#             return Afiliado.objects.get(codAfiliado=pk)
#         except Estudiante.DoesNotExist:
#             raise Http404

#     def get(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = DAfiliadoSerializer(snippet)
#         return Response(serializer.data)

#     def put(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = DAfiliadoSerializer(snippet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class ListaAfiliadoDetalle(APIView):
#     def get(self, request, format = None):
#         snippets = Afiliado.objects.all().order_by('-fecha')
#         serializer = AfiliadoSerializer (snippets, many = True)
#         return Response(serializer.data)


# class ListaAfiliado(APIView):

#     def get(self, request, format = None):
#         snippets = Afiliado.objects.all().order_by('-fecha')
#         serializer = DAfiliadoSerializer (snippets, many = True)
#         return Response(serializer.data)
#     def post(self, request, format=None):
#         serializer = DAfiliadoSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




# class FormAfili(APIView):
#     def get(self, request, format = None):
#         snippets = FORM_DOC.objects.all()
#         serializer = FormaAfiSerializer (snippets, many = True)
#         return Response(serializer.data)


#     def post(self, request, format=None):
#         serializer = FormaAfiSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class DetalleAfiliado(APIView):
#     def get(self, request, pk, format=None):
#         snippets = Afiliado.objects.filter(ci = pk).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)

# class AfiliadoDia(APIView):
#     def get(self, request,fecha,format=None):
#         snippets = Afiliado.objects.filter(fecha = fecha).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)


# class AfiliadodelDia(APIView):
#     def get(self, request,fecha, est,format=None):
#         snippets = Afiliado.objects.filter(estado = est, fecha = fecha).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)

# class AfiliacionPeriodoT(APIView):
#     def get(self, request,fecha1,fecha2,format=None):
#         snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2]).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)

# class AfiliacionPeriodo(APIView):
#     def get(self, request,fecha1,fecha2,est,format=None):
#         snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2], estado= est).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)
# "Carrera"
# class AfiliacioncarreraT(APIView):
#     def get(self, request,fecha1,fecha2,carrera,format=None):
        
#         snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2], ci__Carrera__carrera= carrera).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)

# class Afiliacioncarrera(APIView):
#     def get(self, request,fecha1,fecha2,est, carrera,format=None):
#         snippets = Afiliado.objects.filter(fecha__range=[fecha1, fecha2], estado= est, ci__Carrera__carrera = carrera).order_by('-fecha')
#         serializer = AfiliadoSerializer(snippets, many = True)
#         return Response(serializer.data)


# class IngresarEstudiante(APIView):
#     def post(self, request):
#             ci = request.data.get("ci")
#             apellido_p = request.data.get("apellido_p")
#             apellido_m = request.data.get("apellido_m")
#             nombres = request.data.get("nombres")
#             sexo = request.data.get("sexo")
#             fecha_nac = request.data.get("fecha_nac")
#             telefono = request.data.get("telefono")
#             celular = request.data.get("celular")
#             direccion = request.data.get("direccion")
#             est_civil = request.data.get("est_civil")
#             Carrera = request.data.get("Carrera")
#             matricula = request.data.get("matricula")
#             data = {'ci': ci, 'nombres': nombres, 'apellido_p': apellido_p, 'apellido_m':apellido_m,'sexo':sexo, 'fecha_nac': fecha_nac, 'telefono': telefono, 'celular': celular, 'direccion': direccion, 'est_civil': est_civil, 'Carrera': Carrera, 'matricula': matricula}
#             serializer = EstudiantesSerializer(data=data)
#             if serializer.is_valid():
#                 serializer.save()
#                 return Response(serializer.data, status=status.HTTP_201_CREATED)
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class IngresarAfiliado(APIView):
#     def post(self, request):
#             ci = request.data.get("ci")
#             codAfiliado = request.data.get("codAfiliado")
#             apellidoEsposo = request.data.get("apellidoEsposo")
#             nombre_Referencia= request.data.get("nombre_Referencia")
#             telefono_Referencia= request.data.get("telefono_Referencia")
#             ciudad= request.data.get("ciudad")
#             estado= request.data.get("estado")

#             alergia= request.data.get("alergia")
#             tipo_sangre= request.data.get("tipo_sangre")
#             fecha= request.data.get("fecha")

#             data = {'ci': ci, 'codAfiliado': codAfiliado, 'apellidoEsposo': apellidoEsposo, 'nombre_Referencia': nombre_Referencia, 'telefono_Referencia':telefono_Referencia,'ciudad':ciudad, 'estado': estado,'alergia': alergia, 'tipo_sangre': tipo_sangre, 'fecha':fecha }
#             serializer = AfiliadoSerializer(data=data)
#             if serializer.is_valid():
#                 serializer.save()
#                 return Response(serializer.data, status=status.HTTP_201_CREATED)
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class Destudiantex(APIView):

#     def get(self, request, pk, format=None):
#         snippets = Estudiante.objects.filter(ci = pk)
#         serializer = EstudianteSerializer(snippets, many = True)
#         return Response(serializer.data)




# class EstudianteDetalle(APIView):
#     def get_object(self, pk):
#         try:
#             return Estudiante.objects.get(ci=pk)
#         except Estudiante.DoesNotExist:
#             raise Http404


#     def get(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = EstudiantesSerializer(snippet)
#         return Response(serializer.data)


#     def put(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = EstudiantesSerializer(snippet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#     """BORRA UN REGISTRO"""
#     def delete(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         snippet.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)


# class RegistroAfiliado(APIView):

#     #LISTA TODOS LOS REGISTROS
#     def get(self, request, format=None):
#         snippets = FORM_DOC.objects.all()
#         serializer = FORM_DOCSerializer(snippets, many = True)
#         return Response(serializer.data)



#     #CREA UN NUEVO REGISTRO
#     def post(self, request, format=None):
#         serializer = EstudianteAfiSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# "REGISTROS  DE  ALERGIAS"
# class ListaRegistroAlergias(APIView):
#    #Realiza un listado de los datos
#     def get(self, request,format=None):
#         snippet = Registro_aler.objects.all()
#         serializador = RegistroAlergiasSerializer(snippet, many=True)
#         return Response(serializador.data)
#  #Crea un nuevo registro
#     def post(self, request, format=None):
#         serializer = RegistroAlergiasSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class AlergiasAfiliado(APIView):
#      #Realiza un listado de los datos que sean igual a algo WHERE
#     def get(self, request, pk,format=None):
#         snippet = Registro_aler.objects.filter(afiliado=pk)
#         serializador = RegistraAlergiaJoinSerializer(snippet, many=True)
#         return Response(serializador.data)


# class TBajas(APIView):
#     def get(self, request,format=None):
#         snippet = T_Baja.objects.all()
#         serializador = TBajaSerializer(snippet, many=True)
#         return Response(serializador.data)
#  #Crea un nuevo registro
#     def post(self, request, format=None):
#         serializer = TBajaSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#     #Baja Detallada DE un Afiliado

# class VerBajaAfiliado(APIView):
#     def get(self, request, pk,format=None):
#         snippet = SolicitaBaja.objects.filter(ci=pk)
#         serializador = VerSolicitaBajaSerializer(snippet, many=True)
#         return Response(serializador.data)
#  ## obtener y crear un nuevo registro de Baja deafiliado
# class SolicitarBaja(APIView):
#     def get(self, request,format=None):
#         snippet = SolicitaBaja.objects.all()
#         serializador = SolicitaBajaSerializer(snippet, many=True)
#         return Response(serializador.data)
#  #Crea un nuevo registro
#     def post(self, request, format=None):
#         serializer = SolicitaBajaSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class BajasdelDia(APIView):
#     def get(self, request,fecha,format=None):
#         snippets = SolicitaBaja.objects.filter( fecha = fecha)
#         serializer = VerSolicitaBajaSerializer(snippets, many = True)
#         return Response(serializer.data)

# class BajasPeriodo(APIView):
#     def get(self, request,fecha1,fecha2,format=None):
#         snippets = SolicitaBaja.objects.filter(fecha__range=[fecha1, fecha2])
#         serializer = VerSolicitaBajaSerializer(snippets, many = True)
#         return Response(serializer.data)

# ## obtiene, edita y elimina un registro de baja de afiliado

# class VerEditarBaja(APIView):
#     def get_object(self, pk):
#         try:
#             return SolicitaBaja.objects.get(ci=pk)
#         except Estudiante.DoesNotExist:
#             raise Http404


#     def get(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = SolicitaBajaSerializer(snippet)
#         return Response(serializer.data)


#     def put(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = SolicitaBajaSerializer(snippet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#     """BORRA UN REGISTRO"""
#     def delete(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         snippet.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
