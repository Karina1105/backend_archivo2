from django.contrib import admin

from apps.afiliacion.models import Estudiante, T_Estado_Civil, Afiliado, T_Localidad,T_Alergia_Medicamento,T_Ciudad, T_Baja, SolicitaBaja, T_Tipo_Sangre, Provincia,Carrera, FORM_DOC, Registro_aler, RFORM_DOC, T_Respaldo, Pais, Facultad, Estudianteumsa, T_Parentesco

class TEstudiante(admin.ModelAdmin):
    list_display = ('ci','primer_apellido','segundo_apellido','nombres','sexo','fecha_nac','Carrera','matricula')

class TAfiliado(admin.ModelAdmin):
    list_display = ( 'ci','codAfiliado', 'tipo_sangre', 'estado', 'gestion', 'parentesco')

class TForm(admin.ModelAdmin):
    list_display = ('cod_form', 'ci', 'fecha')

class TRForm(admin.ModelAdmin):
    list_display = ('cod_forr', 'ci', 'fecha')

admin.site.register(Estudiante, TEstudiante)
admin.site.register(T_Estado_Civil)
admin.site.register(Afiliado, TAfiliado)
admin.site.register(T_Localidad)
admin.site.register(T_Alergia_Medicamento)
admin.site.register(T_Ciudad)
admin.site.register(T_Baja)
admin.site.register(T_Parentesco)
admin.site.register(SolicitaBaja)
admin.site.register(T_Tipo_Sangre)
admin.site.register(Provincia)
admin.site.register(Pais)
admin.site.register(FORM_DOC, TForm)
admin.site.register(RFORM_DOC,TRForm)
admin.site.register(Carrera)
admin.site.register(Registro_aler)
admin.site.register(T_Respaldo)
admin.site.register(Facultad)
admin.site.register(Estudianteumsa)
