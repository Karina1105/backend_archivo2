from django.db import models
from ..afiliacion.models import Afiliado
# Create your models here.

class FeriadoEvento(models.Model):
    Tipo = (('Feriado','Feriado'), ('Paro','Paro'))
    fecha = models.DateField()
    asunto = models.CharField(max_length = 50)
    observacion = models.CharField(max_length = 30, blank = True, null = True)
    tipo = models.CharField(max_length = 7, choices = Tipo)
    
    class Meta:
        verbose_name = ("FeriadoParo")
        verbose_name_plural = ("Feriados_Paros")

    def __int__(self):
        return self.id

class T_Especialidad(models.Model):
    Atiende = (('PROMES','PROMES'),('SSU','SSU'))
    id_esp = models.AutoField(primary_key = True)
    especialidad = models.CharField(max_length = 30)
    se_atiende = models.CharField(max_length =6, choices = Atiende)


    class Meta:
        verbose_name = ("Especialdad")
        verbose_name_plural = ("T_Especialidades Medicas")

    def __int__(self):
        return self.id_esp

class Medico(models.Model):
    Sexo= (('Femenino','Femenino'), ('Masculino','Masculino'))
    ci = models.CharField(max_length = 10, primary_key = True)
    nombre = models.CharField(max_length = 30)
    apellido_p = models.CharField(max_length = 15, blank = True)
    apellido_m = models. CharField(max_length = 15, blank = True)
    fecha_nacimiento= models.DateField()
    sexo = models.CharField(max_length = 1, choices = Sexo)
    matricula_medico= models.CharField(max_length = 15)
    especialidad = models.ForeignKey(T_Especialidad, on_delete = models.CASCADE)
    consultorio = models.IntegerField()

    class Meta:
        verbose_name = ("Medico")
        verbose_name_plural = ("Medicos")

    def __str__(self):
        return self.ci

class T_Dia(models.Model):
    id_dia = models.AutoField(primary_key = True)
    dia = models.CharField(max_length = 10)

    class Meta:
        verbose_name_plural =("Dias")

    def __str__(self):
        return self.dia


class T_Hora (models.Model):
    id_hora = models.AutoField(primary_key = True)
    hora = models.TimeField()

    class Meta:
        verbose_name =("Hora")
        
    def __str__(self):
        template = '{0.hora} '
        return template.format(self)

class Horario(models.Model):
    medico = models.ForeignKey(Medico,on_delete = models.CASCADE)
    dia = models.ForeignKey(T_Dia, on_delete = models.CASCADE)
    hora_in = models.TimeField()
    hora_fin = models.TimeField()


class MedicoActividad(models.Model):
    medico = models.ForeignKey(Medico, on_delete = models.CASCADE)
    fecha = models.DateField()
    asunto = models.CharField(max_length = 50)
    hora_i = models.ForeignKey(T_Hora, on_delete = models.CASCADE, related_name="inicio", null = True)
    hora_f = models.ForeignKey(T_Hora, on_delete = models.CASCADE, related_name="fin", null = True)
    class Meta:
        verbose_name = ("ActividadMedico")
        verbose_name_plural = ("ActividadMedicos")

    def __int__(self):
        return self.id


class Ficha(models.Model):
    ESTADO = (('V','Vigente'), ('D','Desactiva'))
    Turno = (('Mañana', 'Mañana'),('Tarde','Tarde'))
    id_ficha = models.AutoField(primary_key = True)
    numero = models.IntegerField()
    hora = models.ForeignKey(T_Hora,on_delete = models.CASCADE)
    medico = models.ForeignKey(Medico, on_delete = models.CASCADE)
    turno = models.CharField(max_length = 7, choices= Turno)
    dia = models.ForeignKey(T_Dia, on_delete= models.CASCADE)
    estado = models.CharField(default = 'V', max_length=1, choices= ESTADO)
    class Meta:
        verbose_name_plural =("Fichas")
        
    def __str__(self):
        template = '{0.numero},{0.medico.nombre} {0.medico.apellido_p}'
        return template.format(self)

class Fichaafiliado(models.Model):
    ESTADO = (('Reservado','Reservado'),('Cancelado','Cancelado'),('Atendido','Atendido'),('Ausente','Ausente'),('Liberada','Liberada'))
    ficha = models.ForeignKey(Ficha, on_delete= models.CASCADE)
    afiliado = models.ForeignKey(Afiliado, on_delete=models.CASCADE)
    fecha = models.DateField()
    estado = models.CharField(max_length = 10, choices = ESTADO, default = 'Reservado')
    
    class Meta:
        verbose_name_plural=("FichaAfiliado")

    def __str__(self):
        template = '{0.afiliado},{0.estado}'
        return template.format(self)




