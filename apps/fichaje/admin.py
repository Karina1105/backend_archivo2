from django.contrib import admin
from apps.fichaje.models import FeriadoEvento, T_Especialidad, T_Dia, Medico, T_Hora, Ficha, Horario, Fichaafiliado, MedicoActividad

class Especialidad(admin.ModelAdmin):
    list_display = ( 'id_esp','especialidad')

class Medicos(admin.ModelAdmin):
    list_display = ( 'ci','sexo','nombre','apellido_p', 'fecha_nacimiento', 'consultorio')

class Horarios(admin.ModelAdmin):
    list_display = ( 'medico','dia', 'hora_in', 'hora_fin')

class Fichas(admin.ModelAdmin):
    list_display = ( 'numero','hora', 'medico', 'turno', 'dia')

class Dia(admin.ModelAdmin):
    list_display = ('id_dia', 'dia')

class Fichaafiliados(admin.ModelAdmin):
    list_display = ('ficha','afiliado','fecha','estado')


admin.site.register(FeriadoEvento)
admin.site.register(T_Especialidad, Especialidad)
admin.site.register(T_Dia, Dia)
admin.site.register(Medico, Medicos)
admin.site.register(T_Hora)
admin.site.register(Ficha, Fichas)
admin.site.register(Horario, Horarios)
admin.site.register(Fichaafiliado,Fichaafiliados)
admin.site.register(MedicoActividad)

# Register your models here.
