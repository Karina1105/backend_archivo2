from django.urls import path
from .views import (
    ListaEspecialidad, ListaDias, ListaMedicos,
    ListaMedicosDetalle, DetalleMedico, DetalleMedicoE, Horarios, 
    ListaHorarios, ListaFichas, FichaD,FichaMedico,
    FichaEspecialidad, 
    FichaAsignada, FichaAfAt, FichaAsignadaD,
    FichaasignadaAf, FichaLibres,FichaasignadaEs,FichaasignadaMe,FichaAsigAffecha, FichaAsigAffechaT,FichasTCarrera,FichasAsig,FichaasignadaMAF,Fichasfecha,Fichasfechaturno,
    ListaFeriados, ListaEventosMedico, Horas,FeriadoX, EventosMedico, FichaMeP, FichasAsP
)

app_name = 'fichaje'
urlpatterns = [
    path('Especialidades/', ListaEspecialidad.as_view(), name = 'ListaEspecialidades'),
    path('Dias/', ListaDias.as_view(), name = 'ListaDias'),
    path('Horas/', Horas.as_view(), name = 'Horas'),
    path('Medicos/', ListaMedicos.as_view(), name = 'Lista Medicos' ),
    path('MedicosD/', ListaMedicosDetalle.as_view(), name = 'Lista Medicos Detalle'),
    path('Detallemedico/<str:pk>/', DetalleMedico.as_view(), name = 'Detalle Medico' ),
    path('Horarios/', Horarios.as_view(), name = 'Horarios'),
    path('HorariosD/', ListaHorarios.as_view(), name = 'Lista horarios'),
    path('Fichas/', ListaFichas.as_view(), name = 'Fichas'),
    path('FichasD/', FichaD.as_view(), name = 'Detalle de Fichas'),
    path('Fichasmedico/<str:pk>/', FichaMedico.as_view(), name = 'Fichas de Medico'),
    path('medicoEsp/<str:esp>/', DetalleMedicoE.as_view(), name = 'Medico Especialidad'),
    path('Fichasespecialidad/<str:esp>/', FichaEspecialidad.as_view(), name = 'Fichas de Especialidad'),
    path('Fichasasignadas/', FichaAsignada.as_view(),name = 'fichas asignadas'),
    path('DetalleFichasAsig/', FichaAsignadaD.as_view(),name = 'fichas asignadas detalle'),
    path('FichasAsigF/<str:fecha>/', Fichasfecha.as_view(),name = 'fichas asignadas en fecha'),
    path('FichasAsigFT/<str:fecha>/<str:turno>/', Fichasfechaturno.as_view(),name = 'fichas asignadas en fecha X y turno Y'),
    path('fichadeafiliado/<str:ci>/<str:fecha>/', FichaasignadaAf.as_view(), name = 'Ficha de afiliado'),
    path('fichaafiliadofechas/<str:ci>/<str:f1>/<str:f2>/', FichaAsigAffecha.as_view(), name = 'Ficha de afiliado'),
    path('fichaafiliadofechasT/<str:ci>/<str:f1>/<str:f2>/', FichaAsigAffechaT.as_view(), name = 'Ficha de afiliado'),
    path('fichaasignadaEs/<str:esp>/<str:fecha1>/<str:fecha2>/', FichaasignadaEs.as_view(), name = 'Fichaasig esp'),
    path('fichaasignadaMe/<str:ci>/<str:fecha>/', FichaasignadaMe.as_view(), name = 'Fichaasig medico'),
    path('fichaasignadaMAF/<str:ci>/<str:me>/<str:fecha>/', FichaasignadaMAF.as_view(), name = 'Ficha maf'),
    path('fichaAsigAte/<str:pk>/', FichaAfAt.as_view(), name = 'Ficha X'),
    path('Fichaslibres/<str:fecha>/<str:esp>/', FichaLibres.as_view(), name = 'Fichas disponibles'),
    path('FeriadoEventos/' , ListaFeriados.as_view(), name = 'Feriados y eventos'),
    path('FeriadoEventoX/<str:fecha>/' , FeriadoX.as_view(), name = 'Feriados y eventos en fecha X'),
    path('MedicoActividad/' , ListaEventosMedico.as_view(), name = 'Actividades de medicos'),
    path('MedicoAct/<str:fecha>/<str:med>/' , EventosMedico.as_view(), name = 'Actividades de medicos'),
    path('FichaAsigPerMedX/<str:ci>/<str:f1>/<str:f2>/' , FichaMeP.as_view(), name = 'FichasAtendidaspo medicoPeriodo'),
    path('FichaAsigPer/<str:f1>/<str:f2>/' , FichasAsP.as_view(), name = 'FichasAtendidasPeriodo'),
    path('FichaAsigMes/<str:pk>/<str:mes>/<str:año>/' , FichasAsig.as_view(), name = 'FichasAtendidasPMes'),
    path('SFicha/<str:f1>/<str:f2>/<str:carrera>/' , FichasTCarrera.as_view(), name = 'FichasAtendidasPeriodo'),

]
