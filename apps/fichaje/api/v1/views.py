from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404, render
from rest_framework import status
from django.db.models import Avg, Count, Min, Sum
from django.db.models import Q
from ....fichaje.models import T_Especialidad, T_Dia, Medico, T_Hora, Ficha, Horario,Fichaafiliado, FeriadoEvento, MedicoActividad
from .serializers import T_DiaSerializer, T_HoraSerializer, EspecialidadSerializer, MedicoSerializer,MedicoESerializer, FichaDSerializer, FichaSerializer, HorarioSerializer, HorarioDSerializer , FeriadoEventoSerializer, FichaAfiliadoSerializer,FichaAfiliadoDSerializer, MedicoActividadSerializer, FichaAfiliadoFSerializer
from ....afiliacion.api.v1.serializers import EstudianteTSerializer
from ....historia.models import Consulta
from ....historia.api.v1.serializers import ConsultaSerializer
class ListaFeriados(APIView):
    def get(self, request, format = None):
        snippets = FeriadoEvento.objects.all()
        serializer = FeriadoEventoSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = FeriadoEventoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FeriadoX(APIView):
    def get(self, request,fecha, format=None):
        snippets = FeriadoEvento.objects.filter(fecha = fecha)
        serializer = FeriadoEventoSerializer(snippets, many = True)
        return Response(serializer.data)


class ListaEventosMedico(APIView):
    def get(self, request, format = None):
        snippets = MedicoActividad.objects.all()
        serializer = MedicoActividadSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = MedicoActividadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EventosMedico(APIView):
    def get(self, request,fecha, med, format=None):
        snippets = MedicoActividad.objects.filter(fecha = fecha, medico = med )
        serializer = MedicoActividadSerializer(snippets, many = True)
        return Response(serializer.data)


class ListaDias(APIView):
    def get(self, request, format = None):
        snippets = T_Dia.objects.all()
        serializer = T_DiaSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = T_DiaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
 
class Horas(APIView):
    def get(self, request, format = None):
        snippets = T_Hora.objects.all().order_by('id_hora')
        serializer = T_HoraSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = T_HoraSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
 
class ListaEspecialidad(APIView):
    def get(self, request, format = None):
        snippets = T_Especialidad.objects.all()
        serializer = EspecialidadSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = EspecialidadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"mostrar Medicos"

class ListaMedicos(APIView):
    def get(self, request, format = None):
        snippets = Medico.objects.all()
        serializer = MedicoSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = MedicoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
 
" Mostrar medicos con detalle "
class ListaMedicosDetalle(APIView):
    def get(self, request, format = None):
        snippets = Medico.objects.all().order_by('especialidad')
        serializer = MedicoESerializer (snippets, many = True)
        return Response(serializer.data)

class DetalleMedico(APIView):
    def get(self, request, pk, format=None):
        snippets = Medico .objects.filter(ci = pk)
        serializer = MedicoESerializer(snippets, many = True)
        return Response(serializer.data)

class DetalleMedicoE(APIView):
    def get(self, request, esp, format=None):
        snippets = Medico .objects.filter(especialidad__especialidad = esp).order_by('consultorio')
        serializer = MedicoESerializer(snippets, many = True)
        return Response(serializer.data)


" Horarios "
class Horarios(APIView):
    def get(self, request, format = None):
        snippets = Horario.objects.all()
        serializer = HorarioSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = HorarioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
 
" lista de Horarios  "
class ListaHorarios(APIView):
    def get(self, request, format = None):
        snippets = Horario.objects.all().order_by('medico')
        serializer = HorarioDSerializer(snippets, many = True)
        return Response(serializer.data)

" Fichas "
class ListaFichas(APIView):
    def get(self, request, format = None):
        snippets = Ficha.objects.all()
        serializer = FichaSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = FichaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


"Fichas detalle con medico"
class FichaD(APIView):
    def get(self, request, format = None):
        snippets = Ficha.objects.all().order_by('medico', 'dia')
        serializer = FichaDSerializer(snippets, many = True)
        return Response(serializer.data)

class FichaMedico(APIView):
    def get(self, request, pk, format=None):
        snippets = Ficha .objects.filter(medico = pk, estado ='V').order_by('dia','numero')
        serializer = FichaDSerializer(snippets, many = True)
        return Response(serializer.data)

class FichaEspecialidad(APIView):
    def get(self, request, esp, format=None):
        snippets = Ficha .objects.filter(medico__especialidad__especialidad = esp)
        serializer = FichaDSerializer(snippets, many = True)
        return Response(serializer.data)

"Fichas Asignadas"
"Todas"
class FichaAsignada(APIView):
    def get(self, request, format = None):
        snippets = Fichaafiliado.objects.all()
        serializer = FichaAfiliadoSerializer(snippets,many = True)
        return Response(serializer.data)

    def post(self, request, format= None):
        serializer = FichaAfiliadoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"Todas las fichas reservadas de la fecha X"
class Fichasfecha(APIView):
    def get(self, request, fecha, format=None):
        snippet = Fichaafiliado.objects.filter(fecha=fecha, estado = 'Reservado')
        serializer = FichaAfiliadoSerializer(snippet,many = True)
        return Response(serializer.data)

"Todas las fichas reservadas de la fecha X turno Y"
class Fichasfechaturno(APIView):
    def get(self, request, fecha, turno, format=None):
        snippet = Fichaafiliado.objects.filter(fecha=fecha, estado = 'Reservado', ficha__turno = turno)
        serializer = FichaAfiliadoSerializer(snippet,many = True)
        return Response(serializer.data)

"Ficha X"
class FichaAfAt(APIView):
    def get_object(self, pk):
        try:
            return Fichaafiliado.objects.get(id=pk)
        except Fichaafiliado.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FichaAfiliadoSerializer(snippet)
        return Response(serializer.data)


    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FichaAfiliadoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

"todas las fichas en detalle"
class FichaAsignadaD(APIView):
    def get(self, request, format=None):
        snippets = Fichaafiliado.objects.all()
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)

"Fichas asignadas entre f1 y f2 del afiliado X"
class FichaAsigAffecha(APIView):
    def get(self, request,ci, f1,f2,format=None):
        snippets =  Fichaafiliado.objects.filter(fecha__range=[f1, f2], afiliado__ci=ci, estado = 'Reservado')
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)

"Fichas asignadas entre f1 y f2 del afiliado X"
class FichaAsigAffechaT(APIView):
    def get(self, request,ci, f1,f2,format=None):
        snippets =  Fichaafiliado.objects.filter(fecha__range=[f1, f2], afiliado__ci=ci)
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)

"verificar si afiliado X ya tiene ficha asignada o fue atendido en f2"
# class FichaAsigAffecha(APIView):
#     def get(self, request,ci, f1,f2,format=None):
#         res = Consulta.objects.filter(fecha=f2, afiliado__ci=ci)
#         lista = res.values_list('afiliado', flat=True)

#         snippets =  Fichaafiliado.objects.filter(fecha__range=[f1, f2], afiliado__ci=ci, estado = 'Reservada').filter(Q(afiliado__in=lista))
#         serializer = FichaAfiliadoDSerializer(snippets, many = True)
#         return Response(serializer.data)

"ficha del afiliado X en fecha Y"

class FichaasignadaAf(APIView):
    def get(self, request, ci,fecha, format=None):
        snippets = Fichaafiliado.objects.filter(afiliado__ci = ci, fecha = fecha)
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)

"fichas asignadas al medico X en fecha  Y"
class FichaasignadaMe(APIView):
    def get(self, request, ci,fecha, format=None):
        snippets = Fichaafiliado.objects.filter(ficha__medico = ci, fecha = fecha)
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)
"Ficha asignada al al medico x en fecha Y del afiliado Z"
class FichaasignadaMAF(APIView):
    def get(self, request, ci, me,fecha, format=None):
        snippets = Fichaafiliado.objects.filter(ficha__medico = me, afiliado__ci=ci, fecha = fecha)
        serializer = FichaAfiliadoSerializer(snippets, many = True)
        return Response(serializer.data)
"fichas asignadas al medico X en el rango de fecha y-z"
class FichaMeP(APIView):
    def get(self, request, ci,f1, f2, format=None):
        snippets = Fichaafiliado.objects.filter(ficha__medico = ci, fecha__range = [f1, f2])
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)

class FichasAsP(APIView):
    def get(self, request, f1, f2, format=None):
        snippets = Fichaafiliado.objects.filter(fecha__range = [f1, f2])
        serializer = FichaAfiliadoDSerializer(snippets, many = True)
        return Response(serializer.data)

class FichasAsig(APIView):
    def get(self, request, pk, mes, año, format=None):
        snippets = Fichaafiliado.objects.filter(afiliado__ci = pk).filter(fecha__month =mes, fecha__year = año).order_by('-fecha')
        serializer = FichaAfiliadoFSerializer(snippets, many = True)
        return Response(serializer.data)

class FichaasignadaEs(APIView):
    def get(self, request, esp,fecha1, fecha2, format=None):
        snippets = Fichaafiliado.objects.filter(fecha__range=[fecha1, fecha2], ficha__medico__especialidad__especialidad=esp)
        serializer = FichaAfiliadoSerializer(snippets, many = True)
        return Response(serializer.data)

class FichasTCarrera(APIView):
    def get(self, request,f1,f2,carrera,format=None):
        snippets = Fichaafiliado.objects.filter(fecha__range = [f1, f2], afiliado__ci__Carrera__carrera= carrera)
        serializer = FichaAfiliadoSerializer(snippets, many = True)
        return Response(serializer.data)

"Fichas disponibles por fecha"

class FichaLibres(APIView):

    def get(self, request, fecha, esp, format=None):
        res = Fichaafiliado.objects.filter(fecha=fecha, ficha__medico__especialidad__especialidad=esp,estado = 'Reservado').order_by('-numero')
        lista = res.values_list('ficha', flat=True)
        resp = Fichaafiliado.objects.filter(fecha=fecha, ficha__medico__especialidad__especialidad=esp,estado = 'Atendido').order_by('-numero')
        lista2 = resp.values_list('ficha', flat=True)
        respp = Fichaafiliado.objects.filter(fecha=fecha, ficha__medico__especialidad__especialidad=esp,estado = 'Cancelado').order_by('-numero')
        lista3 = respp.values_list('ficha', flat=True)
        resppp = Fichaafiliado.objects.filter(fecha=fecha, ficha__medico__especialidad__especialidad=esp,estado = 'Ausente').order_by('-ficha')
        lista4 = resppp.values_list('ficha', flat=True)
        snippets = Ficha.objects.filter(~Q(id_ficha__in= lista),~Q(id_ficha__in= lista2),~Q(id_ficha__in= lista3), ~Q(id_ficha__in= lista4), medico__especialidad__especialidad=esp, estado = "V" ).order_by('medico__consultorio','numero')
        serializer = FichaDSerializer(snippets, many=True)
        return Response(serializer.data)