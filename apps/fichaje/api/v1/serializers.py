from rest_framework import serializers
from ....fichaje.models import T_Especialidad, T_Dia, Medico, T_Hora, Ficha, Horario, Fichaafiliado, FeriadoEvento, MedicoActividad
from ....afiliacion.api.v1.serializers import AfiliadoJSerializer

class FeriadoEventoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeriadoEvento
        fields = '__all__'

class T_DiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Dia
        fields = '__all__'

class T_HoraSerializer(serializers.ModelSerializer):
    class Meta:
        model = T_Hora
        fields = '__all__'

class EspecialidadSerializer(serializers.ModelSerializer):
    class Meta: 
        model = T_Especialidad
        fields = '__all__'

class MedicoESerializer(serializers.ModelSerializer):
    especialidad = EspecialidadSerializer(read_only = True)
    class Meta:
        model = Medico
        fields = '__all__'

class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'

class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields = '__all__'

class MedicoActividadSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicoActividad
        fields = '__all__'

class HorarioDSerializer(serializers.ModelSerializer):
    medico = MedicoESerializer(read_only = True)
    dia = T_DiaSerializer(read_only = True)
    class Meta:
        model = Horario
        fields = '__all__'

class FichaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ficha
        fields = '__all__'

class FichaDSerializer(serializers.ModelSerializer):
    hora = T_HoraSerializer(read_only = True)
    medico = MedicoESerializer(read_only = True)
    dia = T_DiaSerializer(read_only = True)
    class Meta:
        model = Ficha
        fields = '__all__'

class FichaMSerializer(serializers.ModelSerializer):
    medico = MedicoESerializer(read_only = True)
    class Meta:
        model = Ficha
        fields = '__all__'

"Fihas Asignadas"

class FichaAfiliadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fichaafiliado
        fields = '__all__'

class FichaAfiliadoDSerializer(serializers.ModelSerializer):
    afiliado = AfiliadoJSerializer(read_only = True)
    ficha = FichaDSerializer(read_only = True)
    class Meta:
        model = Fichaafiliado
        fields = '__all__'

class FichaAfiliadoFSerializer(serializers.ModelSerializer):
    ficha = FichaMSerializer(read_only = True)
    class Meta:
        model = Fichaafiliado
        fields = '__all__'

class FichaTSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField()
    class Meta:
        model = Fichaafiliado
        fields = ('afiliado_id__ci_id__Carrera_id','total')